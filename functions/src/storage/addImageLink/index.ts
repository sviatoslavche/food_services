import {EventContext} from 'firebase-functions';
import {ObjectMetadata} from 'firebase-functions/lib/providers/storage';

import {initializeAdminFirebaseApp} from '../../utils';

export const addImageLinkHandler = async (event: ObjectMetadata, context: EventContext) => {
  // { bucket: 'food-data-fcda6.appspot.com',
  //     contentType: 'image/jpeg',
  //     crc32c: 'uUM6ug==',
  //     etag: 'CMbK6JqLh+ACEAE=',
  //     generation: '1548355899106630',
  //     id: 'food-data-fcda6.appspot.com/4u4SKlcixq8cUqMIXEGV/authorPhoto/1548355899106630',
  //     kind: 'storage#object',
  //     md5Hash: '/CP1DUcAYbArqGJVWNtKNQ==',
  //     mediaLink: 'https://www.googleapis.com/download/storage/v1/b/food-data-fcda6.appspot.com/o/4u4SKlcixq8cUqMIXEGV%2FauthorPhoto?generation=1548355899106630&alt=media',
  //     metageneration: '1',
  //     name: '4u4SKlcixq8cUqMIXEGV/authorPhoto',
  //     selfLink: 'https://www.googleapis.com/storage/v1/b/food-data-fcda6.appspot.com/o/4u4SKlcixq8cUqMIXEGV%2FauthorPhoto',
  //     size: '1431',
  //     storageClass: 'STANDARD',
  //     timeCreated: '2019-01-24T18:51:39.106Z',
  //     timeStorageClassUpdated: '2019-01-24T18:51:39.106Z',
  //     updated: '2019-01-24T18:51:39.106Z' }

  // { eventId: '367010241326368',
  //     timestamp: '2019-01-24T18:51:39.246Z',
  //     eventType: 'google.storage.object.finalize',
  //     resource:
  //     { service: 'storage.googleapis.com',
  //         name: 'projects/_/buckets/food-data-fcda6.appspot.com/objects/4u4SKlcixq8cUqMIXEGV/authorPhoto',
  //         type: 'storage#object' },
  //     params: {} }

  const logPattern = `[add-image-link][event:${context.eventId}]`;

  console.info(`${logPattern} start`);
  const [postId, fileName] = event.name!.split('/');

  console.info(`${logPattern} initializing app`);
  const adminApp = initializeAdminFirebaseApp();

  const bucket = adminApp.storage().bucket();

  console.info(`${logPattern} getting signed url`);
  const [downloadUrl] = await bucket.file(event.name || '')
    .getSignedUrl({action: 'read', expires: '03-09-2491'});

  console.info(`${logPattern} saving url`);
  await adminApp.firestore().collection('foodItems').doc(postId).update({[`postImages.${fileName}`]: {downloadUrl}});

  console.info(`${logPattern} Record update complete`);
  return event;
};