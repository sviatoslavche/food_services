import {Request, RequestHandler, Response} from "express";
import {initializeAdminFirebaseApp} from "../../../utils";

export const customerFeedback: RequestHandler = async (request: Request, response: Response) => {
  const logPattern = `[customer-feedback]`;

  console.info(`${logPattern} start`);

  const feedback = request.body;

  console.info(`${logPattern} request body: ${JSON.stringify(feedback)}`);

  console.info(`${logPattern} initializing app`);
  const adminApp = initializeAdminFirebaseApp();

  console.info(`${logPattern} saving customer feedback`);

  await adminApp.firestore().collection('feedbacks').add({
    ...feedback,
    createdAt: (new Date()).getTime()
  });

  return response.json({saved: true});
};