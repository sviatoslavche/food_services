import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';

import {customerFeedback} from './customer'

const feedbackAPI: express.Application = express();
const initializedCookieParserMiddleware = cookieParser();
const initializedCorsMiddleware = cors({origin: true});

feedbackAPI.use(initializedCookieParserMiddleware);
feedbackAPI.use(initializedCorsMiddleware);

feedbackAPI.post('/customer', customerFeedback);

export {
  feedbackAPI
};