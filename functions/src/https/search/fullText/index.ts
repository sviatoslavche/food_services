import {Request, Response, RequestHandler} from 'express';
import {get, map} from 'lodash';
import {unflatten} from 'flat'
import {elasticClient, ELASTIC_INDEX, ELASTIC_TYPE} from '../../../utils';
import {getQuery} from '../../../utils/queryBuilder';


export const postText: RequestHandler = async (request: Request, response: Response) => {
  const logPattern = `[search-food-post]`;

  console.info(`${logPattern} start`);

  console.info(`${logPattern} request body: ${JSON.stringify(request.body)}`);

  const searchQuery = getQuery(request.body);

  console.info(`${logPattern} searching query: ${JSON.stringify(searchQuery)}`);

  const searchResult = await elasticClient.search({
    index: ELASTIC_INDEX,
    type: ELASTIC_TYPE,
    filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
    body: searchQuery
  });

  console.info(`${logPattern} we got some result`);

  const minifiedResponse = map(get(searchResult, 'hits.hits', []), entry => {
    const data = get(entry, '_source', {});
    const unflattenedResult: any = unflatten(data, {delimiter: '_', object: true});
    const {postImages, ...postData} = unflattenedResult;

    const images = Object.keys(get(unflattenedResult, 'postImages', {}))
      .reduce((acc: any, key: string) => {
        acc[key] = postImages[key].downloadUrl;
        return acc;
      }, {});

    return {...postData, images}
  });

  return response.json({
    posts: minifiedResponse,
    total: get(searchResult, 'hits.total', 0),
    from: searchQuery.from
  });
};