import * as express from 'express';
import * as cookieParser from 'cookie-parser';
import * as cors from 'cors';

import {postText} from './fullText'

const searchAPI: express.Application = express();
const initializedCookieParserMiddleware = cookieParser();
const initializedCorsMiddleware = cors({origin: true});

searchAPI.use(initializedCookieParserMiddleware);
searchAPI.use(initializedCorsMiddleware);

searchAPI.post('/foodPost', postText);

export {
  searchAPI
};