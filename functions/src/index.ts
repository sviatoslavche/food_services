import {firestore, storage, https} from 'firebase-functions';
import {addImageLinkHandler} from './storage/addImageLink';
import {searchAPI} from './https/search';
import {feedbackAPI} from './https/userFeedback';
import {onCreate, onDelete, onUpdate} from './firestore/syncToElastic';

export const addImageRecords = storage.object().onFinalize(addImageLinkHandler);

export const indexNewRecord = firestore.document('/foodItems/{foodPostId}').onCreate(onCreate);
export const deleteRecord = firestore.document('/foodItems/{foodPostId}').onDelete(onDelete);
export const updateRecord = firestore.document('/foodItems/{foodPostId}').onUpdate(onUpdate);
export const search = https.onRequest(searchAPI);
export const feedback = https.onRequest(feedbackAPI);
