import * as firebaseAdmin from 'firebase-admin';
import * as elastic from 'elasticsearch';
import {flatten} from 'flat';

export const DEFAULT_APP_NAME = 'default';
const initializedAdminApp: any = {};
export const ELASTIC_SERVER_URL = '34.73.185.139/elasticsearch';
export const ELASTIC_USER = 'user';
export const ELASTIC_PASSWORD = 'rfykxKCW6ZCv';
export const ELASTIC_INDEX = 'food';
export const ELASTIC_TYPE = 'posts';

export const initializeAdminFirebaseApp: () => firebaseAdmin.app.App = function () {
  const appIsNotInitialized = !initializedAdminApp[DEFAULT_APP_NAME];

  if (appIsNotInitialized) {
    initializedAdminApp[DEFAULT_APP_NAME] = firebaseAdmin.initializeApp();

    (initializedAdminApp[DEFAULT_APP_NAME] as firebaseAdmin.app.App)
      .firestore()
      .settings({timestampsInSnapshots: true});
  }

  return initializedAdminApp[DEFAULT_APP_NAME];
};

export const elasticClient = new elastic.Client({
  host: `https://${ELASTIC_USER}:${ELASTIC_PASSWORD}@${ELASTIC_SERVER_URL}`,
  log: 'trace'
});

export const prepareElasticData = (data: any) => {
  if (data.postTimeStamp){
    data.postTimeStamp = (new Date(data.postTimeStamp)).getTime();
  }

  if (data.postLastUpdate && data.postLastUpdate.toString().length <= 10){
    data.postLastUpdate = (new Date(data.postLastUpdate * 1e3)).getTime();
  }

  return flatten(data, {delimiter: '_'});
};
