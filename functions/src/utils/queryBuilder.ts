import {size} from 'lodash';

// export const getQuery = (options: any) => {
export const getQuery = ({queryText = '', from = 0, responseSize = 3, sort, postType}: any) => {
  const body: any = {
    query: {
      match_all: {}
    },
    from
  };

  const queryTextExists = size(queryText);
  const validPostTypeExists = postType && (postType === 'offer' || postType === 'review' || postType === 'request');

  //-// Only search terms //-//
  if (queryTextExists && !validPostTypeExists) {
    body.query = {
      match_phrase_prefix: {
        postText: queryText
      }
    };
  }
  //-// Only search terms //-//

  //-// Only post type //-//
  if (!queryTextExists && validPostTypeExists) {
    body.query = {
      term: {
        postType: {value: postType}
      }
    };
  }
  //-// Only post type //-//

  //-// If Both types exists //-//
  if (queryTextExists && validPostTypeExists) {
    body.query = {
      bool: {
        must: [
          {
            term: {
              postType: {value: postType}
            }
          },
          {
            match_phrase_prefix: {
              postText: queryText
            }
          }
        ]
      }
    };
  }
  //-// If Both types exists //-//

  //-// Size //-//
  if (responseSize >= 0) {
    body.size = responseSize;
  }
  //-// Size //-//

  //-// Sorting //-//
  if (sort && sort === 'created') {
    body.sort = [{postTimeStamp: {order: 'desc'}}]
  }
  //-// Sorting //-//

  return body;
};