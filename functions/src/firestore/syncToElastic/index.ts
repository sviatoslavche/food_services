import {Change, EventContext} from "firebase-functions";
import {DocumentSnapshot} from "firebase-functions/lib/providers/firestore";
import {
  elasticClient,
  prepareElasticData,
  ELASTIC_INDEX,
  ELASTIC_TYPE,
} from '../../utils';

export const onCreate = async (snapshot: DocumentSnapshot, context: EventContext) => {
  const {foodPostId} = context.params;
  const logPattern = `[food-item-create][event:${context.eventId}][postId:${foodPostId}]`;

  console.info(`${logPattern} start`);

  const newPost = snapshot.data();
  const id = snapshot.id;

  const elasticObject = prepareElasticData({id, ...newPost});

  console.info(`${logPattern} saving data to elastic`);

  const res = await elasticClient.index({
    index: ELASTIC_INDEX,
    id: id,
    type: ELASTIC_TYPE,
    body: elasticObject
  });

  console.log(res);

  console.info(`${logPattern} entity was saved`);
  return true;
};

export const onDelete = async (snapshot: DocumentSnapshot, context: EventContext) => {
  const {foodPostId} = context.params;
  const logPattern = `[food-item-delete][event:${context.eventId}][postId:${foodPostId}]`;

  console.info(`${logPattern} start`);
  const id = snapshot.id;

  console.info(`${logPattern} deleting data from elastic`);
  const res = await elasticClient.delete({
    index: ELASTIC_INDEX,
    id: id,
    type: ELASTIC_TYPE
  });

  console.log(res);

  console.info(`${logPattern} entity was deleted`);
  return true;
};

export const onUpdate = async (change: Change<DocumentSnapshot>, context: EventContext) => {
  const {foodPostId} = context.params;
  const logPattern = `[food-item-update][event:${context.eventId}][postId:${foodPostId}]`;

  console.info(`${logPattern} start`);
  const updatedPost = change.after.data();
  const id = change.after.id;

  const elasticObject = prepareElasticData({id, ...updatedPost});

  console.info(`${logPattern} updating elastic data`);
  const res = await elasticClient.index({
    index: ELASTIC_INDEX,
    id: id,
    type: ELASTIC_TYPE,
    body: elasticObject
  });

  console.log(res);

  console.info(`${logPattern} entity was updated`);
  return true;
};
